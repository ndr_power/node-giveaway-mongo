let mongoose = require('mongoose');
mongoose.Promise = global.Promise;
mongoose.connect('mongodb://127.0.0.1:27017/nodetest', { useMongoClient: true });
let db = mongoose.connection;
let User = require('./models/user');     
let Giveaway = require('./models/giveaway');
const express = require('express');
const app = express();
const jwt = require('jsonwebtoken');
  const engine = require('ejs-layout')
  , bodyParser = require('body-parser')
  , util = require('util')
  , session = require('express-session')
  , SteamStrategy = require('passport-steam').Strategy
  , passport = require('passport')
  , cookie_parser = require('cookie-parser');
passport.serializeUser(function(user, done) {
  User.find({steamid: user._json.steamid}, (err2, user2) => {
    console.log(123)
    if(err2) console.log( err2 );
    if(user2.length == 0){ // первый раз
      User.create({
        username: user._json.personaname,
        steamid: user._json.steamid,
        avatar: user._json.avatar,
        admin: false
      });
    }else if(user2.length == 1){ // юзер уже заходил
      console.log('is in database')
    }// else WTF?!
  });
  done(null, user);
});

passport.deserializeUser(function(obj, done) {
  done(null, obj);
});
passport.use(new SteamStrategy({
    returnURL: 'http://ndrstudio.ru:3300/auth/steam/return',
    realm: 'http://ndrstudio.ru:3300/',
    apiKey: '659F4465A5BC6B80E037D287A8D94336'
  },
  function(identifier, profile, done) {
    process.nextTick(function () {
      profile.identifier = identifier;
      return done(null, profile);
    });
  }
));

app.use(session({
    secret: 'somethingVerySecret',
    name: 'yo',
    resave: true,
    saveUninitialized: true}));
    app.use(passport.initialize());
app.use(passport.session());
app.set('views', __dirname + '/views');
app.set('view engine', 'ejs');
app.engine('ejs', engine.__express);
app.use(express.static('public'));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookie_parser())
app.use(bodyParser.json())
let steamAuthRoutes = require('./routes/steamauth');
let generalRoutes = require('./routes/general');
let databaseRoutes = require('./routes/database');
let verifyRoutes = require('./routes/verify'); 
app.use('/database', databaseRoutes);
app.use('/auth', steamAuthRoutes);
app.use('/', generalRoutes);
app.listen(3300);
User.find({}, (err, users) => {
    console.log(users);
});
   Date.prototype.addHours = function(h) {    
   this.setTime(this.getTime() + (h*60*60*1000)); 
   return this;   
}
  Giveaway.find({}, (err, giveaways) => {
    console.log('________________ GIVEAWAYS _________________')
    console.log(giveaways)
  })
  // let smth = new Date();
  // Giveaway.create({
  //   name: 'Some kind of giveaway',
  //   tasks: [{linkType: 'fb', instance: 'http://facebook.com/',groupid: '' }],
  //   createdAt: new Date(),
  //   finishDate: smth.addHours(2),
  //   active: true
  // })
