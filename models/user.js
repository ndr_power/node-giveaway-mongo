let mongoose = require('mongoose');
let Schema = mongoose.Schema;
let userSchema = new Schema({
    username: String,
    steamid: {type: Number, unique: true},
    avatar: String,
    giveaways: [
        {
            id:String,
            tasks: [
                    {
                    id: String,
                    linkType: String, // steam, fb, twitter, insta
                    instance: String, // link for group
                    groupid: String, //optional just for steam
                    completed: Boolean
                    }
            ]
        }
    ],
    admin: Boolean, 
});
let User = mongoose.model('User', userSchema);
module.exports = User;