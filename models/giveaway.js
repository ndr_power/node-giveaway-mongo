let mongoose = require('mongoose');
let Schema = mongoose.Schema;
let giveAwaySchema = new Schema({
    name: String,
    tasks: [
        {
        id: String,
        linkType: String, // steam, fb, twitter, insta
        instance: String, // link for group
        groupid: String //optional just for steam
        }
    ],
    createdAt: Date,
    finishDate: Date,
    active: Boolean
});
giveAwaySchema.pre('save', function(next)  {
    this.createdAt = new Date();
    this.tasks.forEach(function(element) {
        element.id = '_' + Math.random().toString(36).substr(2, 9);
    }, this);
    next();
});
let Giveaway = mongoose.model('Giveaway', giveAwaySchema);
module.exports = Giveaway;
