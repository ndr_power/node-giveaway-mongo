let express = require('express')
  , router = express.Router()
  , passport = require('passport')
  , cookie_parser = require('cookie-parser')
  , jwt = require('jsonwebtoken');
const fs = require('fs');
const secretKey = fs.readFileSync('private.key')
router.use(cookie_parser());
router.get('/steam',
  passport.authenticate('steam', { failureRedirect: '/' }),
  function(req, res) {
    res.redirect('/');
});

router.get('/steam/return',
  passport.authenticate('steam', { failureRedirect: '/' }), (req, res) => {
    if(!req.cookies.user){
      let token = jwt.sign({
        personaname: req.user._json.personaname,
        steamid: req.user._json.steamid,
        avatarmedium: req.user._json.avatarmedium
      }, secretKey, {algorithm: 'RS512'});
      console.log(token);
      res.cookie('user', token);
    }
     res.redirect('/');
 });
  router.get('/logout', (req, res) => {
      if(req.user){
        if(req.cookies.user)
          res.clearCookie("user");
      req.logout();
      }
      if(req.cookies.user)
          res.clearCookie("user");
      res.redirect('/');
  });
    module.exports = router;