let express = require('express')
  , router = express.Router()
  , jwt = require('jsonwebtoken');
const fs = require('fs');
const secretKey = fs.readFileSync('private.key')
let mongoose = require('mongoose');
mongoose.Promise = global.Promise;
mongoose.connect('mongodb://127.0.0.1:27017/nodetest', { useMongoClient: true });
let Giveaway = require('../models/giveaway');

router.get('/addGiveaway/:', (req, res) => {
    if(req.user)
      res.render('pages/index', {user: req.user._json});
    else if(req.cookies.user){
      let decodedUser = jwt.decode(req.cookies.user, secretKey, {algorithm: 'RS512'})
      res.render('pages/index', {user: decodedUser});
    }else
      res.render('pages/index', {user: undefined});
});
module.exports = router;