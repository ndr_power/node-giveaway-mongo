let express = require('express')
  , router = express.Router()
  , jwt = require('jsonwebtoken');
const fs = require('fs');
const secretKey = fs.readFileSync('private.key')
let mongoose = require('mongoose');
mongoose.Promise = global.Promise;
mongoose.connect('mongodb://127.0.0.1:27017/nodetest', { useMongoClient: true });
let Giveaway = require('../models/giveaway');
let User = require('../models/user');
let HelperClass = require('../helpers/functions');
let helper = new HelperClass();
router.get('/', (req, res) => {
    Giveaway.find({active: true}, (err, giveaways) => {
      console.log(giveaways);
      if(err) res.render('pages/index', {user: undefined});
      else{
        if(req.user)
          res.render('pages/index', {user: req.user._json, giveaways});
        else if(req.cookies.user){
          let decodedUser = jwt.decode(req.cookies.user, secretKey, {algorithm: 'RS512'})
          res.render('pages/index', {user: decodedUser, giveaways});
        }else
          res.render('pages/index', {user: undefined, giveaways});
    }
    });
    
});
  let kek = 0;
router.get('/giveaway/:id', (req, res) => {
  if(req.user){
    user = req.user
  }else if(req.cookies.user){
              user = jwt.decode(req.cookies.user, secretKey, {algorithm: 'RS512'})
  
  }else{
    user = undefined;
  }
  Giveaway.find({}, (err, giveaways) => {
    if(err) res.render('pages/giveaway', {user,giveaway: undefined});
    for(let i = 0; i<giveaways.length; i++){
      if(giveaways[i]._id == req.params.id){
        res.render('pages/giveaway', {user,giveaway: giveaways[i]});
        break;
      }
      if(i + 1 == giveaways.length){
        res.render('pages/giveaway', {user,giveaway: undefined});
      }
    }
  });
});
router.get('verify/:gid/:tid', (req, res) => {
  if(req.user){
    user = req.user._json;
  }else if(req.cookies.user){
              user = jwt.decode(req.cookies.user, secretKey, {algorithm: 'RS512'})
  }else{
    user = undefined;
  }
  if(user){
    User.find({steamid: user.steamid}, (err, person) => {
      if(person){
        let gres = helper.getGiveawayById(person.giveaways, req.params.gid);
        if(gres){
            let tres = helper.getTaskById(person.giveaways[gres].tasks, req.params.tid);
            if(tres){
              
            }
        }
      }
    });
  }
});
module.exports = router;